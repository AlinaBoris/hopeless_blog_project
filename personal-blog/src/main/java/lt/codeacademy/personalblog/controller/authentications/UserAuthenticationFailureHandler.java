package lt.codeacademy.personalblog.controller.authentications;

import lt.codeacademy.personalblog.services.interfaces.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
public class UserAuthenticationFailureHandler implements AuthenticationFailureHandler {
    @Autowired
    private NotificationService notificationService;

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        displaySignInErrorMessage(httpServletRequest, httpServletResponse);

        redirectStrategy.sendRedirect(httpServletRequest, httpServletResponse, "/user/signin");
    }

    private void displaySignInErrorMessage(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        if (httpServletRequest.getParameter("nickname").isEmpty() ||
                httpServletRequest.getParameter("password").isEmpty()) {
            notificationService.addErrorMessage("For_the_sake_of_coffee! Please_fill_the_form_correctly!");
        } else {
            notificationService.addErrorMessage("Sign_in_process_has_failed...");
        }
    }
}
