package lt.codeacademy.personalblog.controller;

import lt.codeacademy.personalblog.services.interfaces.ErrorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class BlogErrorController implements ErrorController {
    @Override
    public String getErrorPath() {
        return "/error";
    }

    @Autowired
    ErrorService errorService;

    @RequestMapping("/error")
    public String handleError(HttpServletRequest request) {
        return errorService.redirectToErrorPage(request);
    }
}
