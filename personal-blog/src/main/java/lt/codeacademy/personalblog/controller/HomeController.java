package lt.codeacademy.personalblog.controller;

import lt.codeacademy.personalblog.entities.Post;
import lt.codeacademy.personalblog.services.interfaces.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class HomeController {
    @Autowired
    private PostService postService;

    @RequestMapping("/")
    public String homePage(Model model) {
        postService.loadPostsInSidebar(model);
        return "homepage";
    }
}
