package lt.codeacademy.personalblog.controller;

import lt.codeacademy.personalblog.forms.RegistrationForm;
import lt.codeacademy.personalblog.services.interfaces.NotificationService;
import lt.codeacademy.personalblog.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
@RequestMapping("/user")
public class RegisterController {
    @Autowired
    private UserService userService;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @RequestMapping("/register")
    public String register(RegistrationForm registrationForm) {
        return "user/register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerPage(@Valid RegistrationForm registrationForm, BindingResult bindingResult) {
        if (userService.ifExist(registrationForm, bindingResult))
            return "user/register";

        return userService.registerSuccessful(registrationForm);
    }

}
