package lt.codeacademy.personalblog.controller;

import lt.codeacademy.personalblog.forms.SignInForm;
import lt.codeacademy.personalblog.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
@RequestMapping("/user")
public class SignInController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/signin")
    public String signIn(SignInForm signInForm) {
        return "user/signin";
    }

    @RequestMapping(value = "/user/signin", method = RequestMethod.POST)
    public String signPage(@Valid SignInForm signInForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "user/signin";
        }
        return "redirect:/";
    }
}
