package lt.codeacademy.personalblog.controller;

import lt.codeacademy.personalblog.entities.Post;
import lt.codeacademy.personalblog.services.interfaces.NotificationService;
import lt.codeacademy.personalblog.services.interfaces.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class PostController {
    @Autowired
    private PostService postService;

    @Autowired
    private NotificationService notificationService;

    @RequestMapping("/post/view/{id}")
    public String view(
            @PathVariable("id") Long id, Model model) {
        return postService.showPostById(id, model);
    }

    @RequestMapping("/post")
    public String listPost(Model model){
        model.addAttribute("post", postService.findAll());
        return "post";
    }

    @RequestMapping("/post/create")
    public String create (Post post){
        return "post/create";
    }

    @RequestMapping(value = "/post/create", method = RequestMethod.POST)
    public String createPost (@Valid Post post, BindingResult bindingResult){
        if(postService.checkNewPost(post, bindingResult)){
            return "post/create";
        }
        return postService.createNewPost(post);
    }

    @RequestMapping("/post/delete/{id}")
    public String getElementToDelete(@PathVariable("id") Long id, Model model) {
        return postService.confirmDeletePostById(id, model);
    }

    @RequestMapping(value = "/post/delete/{id}", method = RequestMethod.DELETE)
    public String deletePost(@PathVariable("id") Long id) {
        return postService.deletePostById(id);
    }

    @RequestMapping("/post/edit/{id}")
    public String getElementToEdit(@PathVariable("id") Long id, Model model) {
        return postService.findPostById(id, model);
    }

    @RequestMapping(value = "edit", method = RequestMethod.PATCH)
    public String editPost(@Valid Post post, BindingResult bindingResult) {
        return postService.validateUserEditPermissions(post, bindingResult);
    }
}