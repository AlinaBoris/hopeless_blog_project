package lt.codeacademy.personalblog.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 29, unique = true)
    private String nickname;

    @Column(length = 50)
    private String password;

    @Column(length = 50)
    private String userName;

    @OneToMany(mappedBy = "author")
    private Set<Post> posts = new HashSet<>();

    public User(){}

    public User(String nickname, String userName){
        this.nickname = nickname;
        this.userName = userName;
    }

    public User(Long id, String nickname, String userName) {
        this.id = id;
        this.nickname = nickname;
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "User{ id=" + id +
                ", nickname='" + nickname + '\'' +
                ", password='" + password + '\'' +
                ", userName='" + userName + '\'' + '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Set<Post> getPosts() {
        return posts;
    }

    public void setPosts(Set<Post> posts) {
        this.posts = posts;
    }
}