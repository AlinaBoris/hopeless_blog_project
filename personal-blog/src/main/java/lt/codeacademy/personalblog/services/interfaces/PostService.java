package lt.codeacademy.personalblog.services.interfaces;

import lt.codeacademy.personalblog.entities.Post;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;

import javax.validation.Valid;
import java.util.List;

public interface PostService {
    List<Post> findAll();
    List<Post> findLatestFive();
    Post findById(Long id);
    Post create(Post post);
    Post edit(Post post);
    void deleteById(Long id);

    boolean checkNewPost(@Valid Post post, BindingResult bindingResult);
    String createNewPost(@Valid Post post);

    String deletePostById(@PathVariable("id") Long id);
    String confirmDeletePostById(@PathVariable("id") Long id, Model model);

    String showPostById(@PathVariable("id") Long id, Model model);
    String findPostById(@PathVariable("id") Long id, Model model);

    void loadPostsInSidebar(Model model);

    String validateUserEditPermissions(@Valid Post post, BindingResult bindingResult);
}
