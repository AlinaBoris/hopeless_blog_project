package lt.codeacademy.personalblog.services.enums;

public enum NotificationMessageType {
    ERROR,
    INFO;
}
