package lt.codeacademy.personalblog.services.interfaces;

public interface NotificationService {
    void addErrorMessage(String message);
    void addInfoMessage(String message);
}
