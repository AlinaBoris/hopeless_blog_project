package lt.codeacademy.personalblog.services;

import lt.codeacademy.personalblog.services.enums.NotificationMessageType;
import lt.codeacademy.personalblog.services.interfaces.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Service
public class NotificationServiceImpl implements NotificationService {
    public static String NOTIFY_MESSAGE = "blogNotificationMessages";

    @Autowired
    private HttpSession httpSession;

    @Override
    public void addErrorMessage(String message) {
        addNotificationMessage(NotificationMessageType.ERROR, message);
    }

    @Override
    public void addInfoMessage(String message) {
        addNotificationMessage(NotificationMessageType.INFO, message);
    }

    private void addNotificationMessage(NotificationMessageType type, String message) {
        List<NotificationMessage> messageList = (List<NotificationMessage>)
                httpSession.getAttribute(NOTIFY_MESSAGE);

        if (messageList == null) {
            messageList = new ArrayList<NotificationMessage>();
        }

        messageList.add(new NotificationMessage(type, message));
        httpSession.setAttribute(NOTIFY_MESSAGE, messageList);
    }

    public class NotificationMessage {
        NotificationMessageType type;
        String text;

        public NotificationMessage(NotificationMessageType type, String text) {
            this.type = type;
            this.text = text;
        }

        public NotificationMessageType getType() {
            return type;
        }

        public String getText() {
            return text;
        }
    }
}
