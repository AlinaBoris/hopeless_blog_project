package lt.codeacademy.personalblog.services;

import lt.codeacademy.personalblog.entities.User;
import lt.codeacademy.personalblog.forms.RegistrationForm;
import lt.codeacademy.personalblog.repositories.UserRepository;
import lt.codeacademy.personalblog.services.interfaces.NotificationService;
import lt.codeacademy.personalblog.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.Optional;

@Service
@Primary
public class UserServiceJpaImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private NotificationService notificationService;


    @Override
    public boolean ifExist(RegistrationForm registrationForm, BindingResult bindingResult) {
        Optional<User> userExistInDataBase = Optional.ofNullable(findByNickname(registrationForm.getNickname()));
        if (bindingResult.hasErrors() || userExistInDataBase.isPresent()) {
            notificationService.addErrorMessage("Due_to_some_unknown_forces, this_user_registration_failed!");
            return true;
        }
        return false;
    }

    @Override
    public List<User> findAll() {
        return this.userRepository.findAll();
    }

    @Override
    public User findById(Long id) {
        return this.userRepository.findOne(id);
    }

    @Override
    public User create(User user) {
        return this.userRepository.save(user);
    }

    @Override
    public User edit(User user) {
        return this.userRepository.save(user);
    }

    @Override
    public void deleteById(Long id) {
        this.userRepository.delete(id);
    }

    @Override
    public User findByNickname(String nickname) {
        return this.userRepository.findByNickname(nickname);
    }

    @Override
    public String registerSuccessful(RegistrationForm registrationForm) {
        User user = new User();
        user.setNickname(registrationForm.getNickname());
        user.setPassword(bCryptPasswordEncoder.encode(registrationForm.getPassword()));
        user.setUserName(registrationForm.getUserName());
        create(user);

        notificationService.addInfoMessage("Registration_successful! Welcome_to_the_coffee_lover_squad!");
        return "redirect:/";
    }
}
