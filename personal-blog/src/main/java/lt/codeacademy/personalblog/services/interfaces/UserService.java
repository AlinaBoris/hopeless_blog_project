package lt.codeacademy.personalblog.services.interfaces;

import lt.codeacademy.personalblog.entities.User;
import lt.codeacademy.personalblog.forms.RegistrationForm;
import org.springframework.validation.BindingResult;

import javax.validation.Valid;
import java.util.List;

public interface UserService {
    boolean ifExist(@Valid RegistrationForm registrationForm, BindingResult bindingResult);

    List<User> findAll();

    User findById(Long id);

    User create(User user);

    User edit(User user);

    void deleteById(Long id);

    User findByNickname(String nickname);

    String registerSuccessful(@Valid RegistrationForm registrationForm);
}
