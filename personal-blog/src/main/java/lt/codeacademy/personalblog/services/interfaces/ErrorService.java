package lt.codeacademy.personalblog.services.interfaces;

import javax.servlet.http.HttpServletRequest;

public interface ErrorService {
    String redirectToErrorPage(HttpServletRequest request);
}
