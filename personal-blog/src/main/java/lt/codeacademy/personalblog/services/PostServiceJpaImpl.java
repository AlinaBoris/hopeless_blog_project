package lt.codeacademy.personalblog.services;

import lt.codeacademy.personalblog.entities.Post;
import lt.codeacademy.personalblog.entities.User;
import lt.codeacademy.personalblog.repositories.PostRepository;
import lt.codeacademy.personalblog.services.interfaces.NotificationService;
import lt.codeacademy.personalblog.services.interfaces.PostService;
import lt.codeacademy.personalblog.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;
import static java.util.stream.Collectors.toList;

@Service
public class PostServiceJpaImpl implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserService userService;

    public String signedInUser;

    @Override
    public List<Post> findAll() {
        return this.postRepository.findAll();
    }

    @Override
    public List<Post> findLatestFive() {
        return this.postRepository.findLatestFive(new PageRequest(0, 5));
    }

    @Override
    public Post findById(Long id) {
        return this.postRepository.findOne(id);
    }

    @Override
    public Post create(Post post) {
        return this.postRepository.save(post);
    }

    @Override
    public Post edit(Post post) {
        return this.postRepository.save(post);
    }

    @Override
    public void deleteById(Long id) {
        this.postRepository.delete(id);
    }

    @Autowired
    private NotificationService notificationService;

    @Override
    public boolean checkNewPost(Post post, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return true;
        }

        if (post.getTitle().isEmpty() || post.getBody().isEmpty()) {
            notificationService.addErrorMessage("Please_fill_all_required_fields!");
            return true;
        }
        return false;
    }

    @Override
    public String createNewPost(Post post) {
        signedInUser = SecurityContextHolder.getContext().getAuthentication().getName();

        User user = userService.findByNickname(signedInUser);
        post.setAuthor(user);

        create(post);
        notificationService.addInfoMessage("Post_successfully_created!");

        return "redirect:/post";
    }

    @Override
    public String deletePostById(Long id) {
        signedInUser = SecurityContextHolder.getContext().getAuthentication().getName();

        if (findById(id).getAuthor().getNickname().equals(signedInUser)) {
            deleteById(id);
            notificationService.addInfoMessage("Post_deleted_successfully!");
            return "redirect:/post";
        }
        notificationService.addErrorMessage("Delete without successful! It's not your post.");

        return "redirect:/post";
    }

    @Override
    public String confirmDeletePostById(Long id, Model model) {
        Post post = findById(id);

        if (post == null) {
            notificationService.addErrorMessage(
                    "Error!This_Coffee_post_is_not_found_" + id + "!\n"
                            + "Go_and_grab_some_more_coffee! It_might_help!");
            return "redirect:/post";
        }

        model.addAttribute("post", post);
        return "/post/delete";
    }

    @Override
    public String showPostById(Long id, Model model) {
        Post post = findById(id);

        if (post == null) {
            notificationService.addErrorMessage(
                    "Error!This_Coffee_post_is_not_found_" + id + "!\n"
                            + "Go_and_grab_some_more_coffee! It_might_help!");
            return "redirect:/";
        }

        model.addAttribute("post", post);
        return "/post/view";
    }


    @Override
    public String findPostById(Long id, Model model) {
        Post post = findById(id);

        if (post == null) {
            notificationService.addErrorMessage(
                    "Error!This_Coffee_post_is_not_found_" + id + "!\n"
                            + "Go_and_grab_some_more_coffee! It_might_help!");
            return "redirect:/post";
        }

        model.addAttribute("post", post);
        return "/post/edit";
    }

    @Override
    public void loadPostsInSidebar(Model model) {
        List<Post> latestFivePosts = findLatestFive();
        model.addAttribute("latestFivePosts", latestFivePosts);

        List<Post> latestPosts = latestFivePosts.stream()
                .limit(5).collect(toList());
        model.addAttribute("latestPosts", latestPosts);
    }


    @Override
    public String validateUserEditPermissions(Post post, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            notificationService.addInfoMessage("No_no_no! This_edit_was_not_successful_at_all!");
            return "/post/edit";
        }

        if (post.getTitle().isEmpty() || post.getBody().isEmpty()) {
            notificationService.addErrorMessage("Please_fill_all_required_fields!");
            return "/post/edit";
        }

        signedInUser = SecurityContextHolder.getContext().getAuthentication().getName();

        if (post.getAuthor().getNickname().equals(signedInUser)) {
            edit(post);
            notificationService.addInfoMessage("Well_done! Now_the_post_was_successfully_changed!");
            return "redirect:/post";
        }
        notificationService.addErrorMessage("No_way... Don_not_try_to_change_another_coffee_lover's_post. " +
                "It_does_not_work_that_way!");

        return "redirect:/post";
    }
}
