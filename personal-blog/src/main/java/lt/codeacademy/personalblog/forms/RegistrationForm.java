package lt.codeacademy.personalblog.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class RegistrationForm {

    @Size(min = 3, max = 29, message = "Size_of_nickname_should_be_in_range_from_3_to_29!")
    private String nickname;


    @Size(min = 5, max = 50, message = "Size_should_be_in_range_from_5_to_50!")
    @NotNull
    private String password;

    @NotNull
    private String userName;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
