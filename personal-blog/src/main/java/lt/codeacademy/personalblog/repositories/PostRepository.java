package lt.codeacademy.personalblog.repositories;

import lt.codeacademy.personalblog.entities.Post;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import java.util.List;

public interface PostRepository extends JpaRepository<Post, Long> {
    @Query("SELECT post FROM Post post LEFT JOIN FETCH post.author ORDER BY post.date DESC")
    List<Post> findLatestFive(Pageable pageable);
}
